//
//  EatenFood+CoreDataProperties.h
//  CoreDataFoodJournal
//
//  Created by James Cash on 21-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EatenFood.h"

NS_ASSUME_NONNULL_BEGIN

@class Foodstuff;
@interface EatenFood (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *eatenAt;
@property (nullable, nonatomic, retain) Foodstuff *eatenFood;

@end

NS_ASSUME_NONNULL_END
