//
//  ViewController.m
//  CoreDataFoodJournal
//
//  Created by James Cash on 21-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "EatenFood.h"
#import "Foodstuff.h"
#import "AppDelegate.h"

@interface ViewController ()
{
    NSFetchedResultsController *ctrl;
}
@property (weak, nonatomic) IBOutlet UITextField *foodNameField;
@property (weak, nonatomic) IBOutlet UITextField *foodCaloriesField;
@property (weak, nonatomic) IBOutlet UITextField *eatenFoodNameField;
@property (weak, nonatomic) IBOutlet UITableView *eatenFoodTable;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *appd = [UIApplication sharedApplication].delegate;
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:@"EatenFood"];
    req.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"eatenAt" ascending:NO]];
    ctrl = [[NSFetchedResultsController alloc] initWithFetchRequest:req managedObjectContext:appd.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    [ctrl performFetch:nil];
//    ctrl.delegate = self;
    // TODO: implement NSFetchedResultsControllerDelegate to reload the table view when data changes
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveEatenFood:(id)sender {
    NSDate *now = [NSDate date];
    NSString *foodName = self.eatenFoodNameField.text;
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"Foodstuff"];
    req.predicate = [NSPredicate predicateWithFormat:@"name = '%@'", foodName];
    req.fetchLimit = 1;
    AppDelegate *appd = [UIApplication sharedApplication].delegate;
    NSError *err = nil;
    NSArray *results = [appd.managedObjectContext executeFetchRequest:req error:&err];
    Foodstuff *food = [results firstObject];
    NSLog(@"Eating %@", food);
    EatenFood *eaten = [[EatenFood alloc] initWithEntity:[NSEntityDescription entityForName:@"EatenFood" inManagedObjectContext:appd.managedObjectContext] insertIntoManagedObjectContext:appd.managedObjectContext];
    eaten.eatenAt = now;
    eaten.eatenFood = food;
    [appd saveContext];
}

- (IBAction)saveNewFood:(id)sender {
    NSString *foodName = self.foodNameField.text;
    NSNumber *calories = [NSNumber numberWithDouble:self.foodCaloriesField.text.doubleValue];
    AppDelegate *appd = [UIApplication sharedApplication].delegate;

    Foodstuff *newFood = [[Foodstuff alloc] initWithEntity:[NSEntityDescription entityForName:@"Foodstuff" inManagedObjectContext:appd.managedObjectContext] insertIntoManagedObjectContext:appd.managedObjectContext];
    newFood.name = foodName;
    newFood.calories = calories;
    [appd saveContext];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ctrl.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EatenFood *eaten = [ctrl objectAtIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eatenFoodCell"];
    UILabel *label = (UILabel*)[cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"At %@: %@: %@C",
                  eaten.eatenAt, eaten.eatenFood.name, eaten.eatenFood.calories];
    return cell;
}
@end
