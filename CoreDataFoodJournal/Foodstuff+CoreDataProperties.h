//
//  Foodstuff+CoreDataProperties.h
//  CoreDataFoodJournal
//
//  Created by James Cash on 21-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff.h"

NS_ASSUME_NONNULL_BEGIN

@interface Foodstuff (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *calories;

@end

NS_ASSUME_NONNULL_END
