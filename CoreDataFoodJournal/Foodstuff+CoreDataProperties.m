//
//  Foodstuff+CoreDataProperties.m
//  CoreDataFoodJournal
//
//  Created by James Cash on 21-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff+CoreDataProperties.h"

@implementation Foodstuff (CoreDataProperties)

@dynamic name;
@dynamic calories;

@end
